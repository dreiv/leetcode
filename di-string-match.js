/**
 * @param {string} str
 * @return {number[]}
 */
const diStringMatch = str => {
	const result = []
	let up = 0,
		down = str.length

	for (let i = 0; i <= str.length; i++) {
		result.push(str[i] === 'I' ? up++ : down--)
	}

	return result
}

console.log(diStringMatch('IDID'))
console.log(diStringMatch('III'))
console.log(diStringMatch('DDI'))
