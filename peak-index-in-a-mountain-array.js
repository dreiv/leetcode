/**
 * @param {number[]} arr
 * @return {number}
 */
const peakIndexInMountainArray = arr =>
	arr.indexOf(Math.max(...arr))

console.log(peakIndexInMountainArray([0, 1, 0]))
console.log(peakIndexInMountainArray([0, 2, 1, 0]))
