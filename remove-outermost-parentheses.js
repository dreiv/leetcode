/**
 * @param {string} S
 * @return {string}
 */
var removeOuterParentheses = function (S) {
	let result = ''
	let nestLevel = 0

	for (char of S) {
		if (char == '(') {
			if (nestLevel++ == 0) continue
		} else if (nestLevel-- == 1) continue

		result += char
	}

	return result
}

console.log(removeOuterParentheses('(()())(())'))
console.log(removeOuterParentheses('(()())(())(()(()))'))
console.log(removeOuterParentheses('()()'))
