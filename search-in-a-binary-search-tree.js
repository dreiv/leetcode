/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} val
 * @return {TreeNode}
 */
const searchBST = (root, val, found = null) => {
	if (root.val === val) return root
	if (!found && root.left) {
		found = searchBST(root.left, val)
	}
	if (!found && root.right) {
		found = searchBST(root.right, val)
	}

	return found
}

const tree = {
	val: 4,
	left: {
		val: 2,
		left: {
			val: 1,
		},
		right: {
			val: 3,
		},
	},
	right: {
		val: 7,
	},
}

console.log(searchBST(tree, 2))
