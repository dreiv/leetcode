const findAnagramMappings = (A, B) => {
	const map = {}

	B.forEach((el, idx) => map[el] = idx)
	return A.map(el => map[el])
}

console.log(findAnagramMappings([12, 28, 46, 32, 50], [50, 12, 32, 46, 28]))
