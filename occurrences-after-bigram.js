/**
 * @param {string} text
 * @param {string} first
 * @param {string} second
 * @return {string[]}
 */
const findOcurrences = (text, first, second) => {
	const words = text.split(" ")
	const result = []

	for (let i = 2; i < words.length; i++) {
		if (words[i - 2] === first && words[i - 1] === second) {
			result.push(words[i])
		}
	}

	return result
}

console.log(
	findOcurrences("alice is a good girl she is a good student", "a", "good")
)
console.log(findOcurrences("we will we will rock you", "we", "will"))
