/**
 * @param {number[][]} img
 * @return {number[][]}
 */
const flipAndInvertImage = img =>
	img.map(row =>
		row
			.reverse()
			.map(el => 1 - el)
	)

console.log(flipAndInvertImage([
	[1, 1, 0],
	[1, 0, 1],
	[0, 0, 0]
])) // [ [ 1, 0, 0 ], [ 0, 1, 0 ], [ 1, 1, 1 ] ]