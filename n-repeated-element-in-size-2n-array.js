/**
 * @param {number[]} arr
 * @return {number}
 */
const repeatedNTimes = arr =>
	arr.find((a, index, array) => array.indexOf(a) !== index)

console.log(repeatedNTimes([1, 2, 3, 3]))