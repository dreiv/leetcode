/**
 * @param {number[]} arr
 * @return {number[]}
 */
const sortedSquares = arr => {
	let result = []
	let up = 0, down = arr.length - 1

	while (up <= down) {
		if (Math.abs(arr[up]) > Math.abs(arr[down])) {
			result.push(arr[up++] ** 2)
		} else {
			result.push(arr[down--] ** 2)
		}
	}

	return result.reverse()
}

console.log(sortedSquares([-4, -1, 0, 3, 10]))