/**
 * @param {string[]} arr
 * @return {number}
 */
const minDeletionSize = arr => {
	let count = 0

	for (let i = 0; i < arr[0].length; i++) {
		for (let j = 1; j < arr.length; j++) {
			if (arr[j][i] < arr[j - 1][i]) {
				count++
				break
			}
		}
	}

	return count
}

console.log(minDeletionSize(['cba', 'daf', 'ghi']))
console.log(minDeletionSize(['a', 'b']))
console.log(minDeletionSize(['zyx', 'wvu', 'tsr']))
