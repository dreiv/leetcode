/**
 * @param {number[]} arr
 * @return {number[]}
 */
const sortArrayByParity = arr =>
	arr.sort((a, b) => a % 2 - b % 2)

console.log(sortArrayByParity([3, 1, 2, 4]))