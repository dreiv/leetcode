/**
 * @param {number} left
 * @param {number} right
 * @return {number[]}
 */
const selfDividingNumbers = (left, right) => {
	const results = []

	for (let i = left; i <= right; i++) {
		let current = i.toString()
		let isGood = true

		for (let y = 0; y < current.length; y++) {
			if (i % parseInt(current[y]) !== 0) {
				isGood = false
				break
			}
		}

		if (isGood) {
			results.push(i)
		}
	}

	return results
}

console.log(selfDividingNumbers(1, 22))
