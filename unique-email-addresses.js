/**
 * @param {string[]} emails
 * @return {number}
 */
const numUniqueEmails = emails =>
	new Set(
		emails
			.map(mail => mail.split('@'))
			.map(([
				local,
				domain
			]) => `${local.replace(/\+.*$|\./g, '')}@${domain}`)
	).size

console.log(
	numUniqueEmails([
		'test.email+alex@leetcode.com',
		'test.e.mail+bob.cathy@leetcode.com',
		'testemail+david@lee.tcode.com',
	])
)
