const isPalindrome = x => {
	if (x < 0 || (x % 10 === 0 && x !== 0)) {
		return false
	}
	let input = x

	let reverse = 0
	while (input > reverse) {
		reverse = reverse * 10 + (input % 10)
		input = Math.trunc(input / 10)
	}

	return input === reverse || input === Math.trunc(reverse / 10)
}

console.log(isPalindrome(121))
// true
console.log(isPalindrome(-121))
// true
console.log(isPalindrome(10))
// true
