/**
 * @param {number[][]} grid
 * @return {number}
 */
const topArea = grid =>
	grid.reduce((acc, curr) => acc + curr.filter(item => item > 0).length, 0)

/**
 * @param {number[][]} grid
 * @return {number}
 */
const frontArea = grid =>
	grid[0].reduce(
		(acc, _, i) => (acc += Math.max(...grid.map(arr => arr[i]))),
		0
	)

/**
 * @param {number[][]} grid
 * @return {number}
 */
const sideArea = grid =>
	grid.reduce((acc, curr) => (acc += Math.max(...curr)), 0)

/**
 * @param {number[][]} grid
 * @return {number}
 */
const projectionArea = grid => {
	return topArea(grid) + frontArea(grid) + sideArea(grid)
}

console.log(projectionArea([[2]]))
console.log(projectionArea([[1, 2], [3, 4]]))
console.log(projectionArea([[1, 0], [0, 2]]))
console.log(projectionArea([[1, 1, 1], [1, 0, 1], [1, 1, 1]]))
