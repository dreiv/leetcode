/**
 * @param {number[]} arr
 * @return {number[]}
 */
const sortArrayByParityII = arr => {
	const result = []
	let odd = 0,
		even = 0

	while (odd < arr.length && even < arr.length) {
		while (arr[odd] % 2 !== 0) odd++
		result.push(arr[odd++])

		while (arr[even] % 2 === 0) even++
		result.push(arr[even++])
	}

	return result
}

console.log(sortArrayByParityII([4, 2, 5, 7]))
