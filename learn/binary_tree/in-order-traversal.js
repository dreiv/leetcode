/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
inOrderTraversal = root => {
	if (!root) return []
	const result = []

	;(iterate = node => {
		if (!node) return

		iterate(node.left)
		result.push(node.val)
		iterate(node.right)
	})(root)

	return result
}

const tree = {
	val: 10,
	left: {
		val: 5,
		left: {
			val: 3,
		},
		right: {
			val: 7,
		},
	},
	right: {
		val: 15,
		right: {
			val: 18,
		},
	},
}

console.log(inOrderTraversal(tree))
