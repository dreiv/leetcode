/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
isSymmetric = root => {
	if (!root) return true

	return (isSame = (left, right) => {
		if (!left && !right) return true

		if (!left || !right || right.val !== left.val) {
			return false
		}

		return isSame(left.left, right.right)
			&& isSame(left.right, right.left)
	})(root.left, root.right)
}

const symmetricalTree = {
	val: 1,
	left: {
		val: 2,
		left: {
			val: 3,
		},
		right: {
			val: 4,
		},
	},
	right: {
		val: 2,
		left: {
			val: 4,
		},
		right: {
			val: 3,
		},
	},
}

const asymmetricalTree = {
	val: 1,
	left: {
		val: 2,
		right: {
			val: 3,
		},
	},
	right: {
		val: 2,
		right: {
			val: 3,
		},
	},
}

console.log(isSymmetric(symmetricalTree))
console.log(isSymmetric(asymmetricalTree))
