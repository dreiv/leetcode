/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
maxDepth = root => {
	if (!root) return 0
	let max = 0

	;(iterate = (node, level) => {
		if (!node) return

		if (level > max) max = level
		iterate(node.left, level + 1)
		iterate(node.right, level + 1)
	})(root, 1)

	return max
}

const tree = {
	val: 10,
	left: {
		val: 5,
		left: {
			val: 3,
			left: {
				val: 1,
			},
		},
		right: {
			val: 7,
		},
	},
	right: {
		val: 15,
		right: {
			val: 18,
		},
	},
}

console.log(maxDepth(tree))
