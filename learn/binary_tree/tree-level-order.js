/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
levelOrder = root => {
	if (!root) return []

	const result = []
	;(search = (node, level) => {
		if (!node) return

		if (result.length < level) {
			result.push([])
		}
		const arr = result[level - 1]
		arr.push(node.val)
		search(node.left, level + 1)
		search(node.right, level + 1)
	})(root, 1)

	return result
}

const tree = {
	val: 10,
	left: {
		val: 5,
		left: {
			val: 3,
		},
		right: {
			val: 7,
		},
	},
	right: {
		val: 15,
		right: {
			val: 18,
		},
	},
}

console.log(levelOrder(tree))
