/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {boolean}
 */
countUniVal = (root, count = 0) => {
	if (!root) return count
	if (!root.left != !root.right) count++

	return countUniVal(root.left, count) + countUniVal(root.right, count)
}

const tree = {
	val: 5,
	left: {
		val: 4,
		left: {
			val: 11,
			left: {
				val: 7,
			},
			right: {
				val: 2,
			},
		},
	},
	right: {
		val: 8,
		left: {
			val: 13,
		},
		right: {
			val: 4,
			right: {
				val: 1,
			},
		},
	},
}

console.log(countUniVal(tree))
