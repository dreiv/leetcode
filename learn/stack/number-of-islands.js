/**
 * @param {character[][]} grid
 * @return {number}
 */
numIslands = grid => {
	let count = 0

	const traverseIsland = (i, j, grid) => {
		const stack = [[i, j]]

		while (stack.length) {
			const [i, j] = stack.pop()

			if (
				i >= 0 &&
				i < grid.length &&
				j >= 0 &&
				j < grid[0].length &&
				grid[i][j] === '1'
			) {
				grid[i][j] = '2'
				stack.push([i + 1, j])
				stack.push([i - 1, j])
				stack.push([i, j + 1])
				stack.push([i, j - 1])
			}
		}
	}

	for (let i = 0; i < grid.length; i++) {
		for (let j = 0; j < grid[0].length; j++) {
			if (grid[i][j] === '1') {
				traverseIsland(i, j, grid)
				count++
			}
		}
	}

	return count
}

const example = [
	['1', '1', '0', '0', '1'],
	['1', '1', '0', '0', '0'],
	['0', '0', '0', '0', '0'],
	['1', '0', '0', '1', '1'],
	['1', '0', '0', '1', '1'],
]

console.log(numIslands(example))
