/**
 * Initialize your data structure here. Set the size of the queue to be k.
 * @param {number} k
 */
var MyCircularQueue = function(k) {
	this.queue = new Array(k)
		.fill(null)

	this.head = -1
	this.tail = -1
}

/**
 * Insert an element into the circular queue. Return true if the operation is successful.
 * @param {number} value
 * @return {boolean}
 */
MyCircularQueue.prototype.enQueue = function(value) {
	if (!this.isFull()) {
		this.changeTailPoint()
		this.queue[this.tail] = value

		return true
	}

	return false
}

/**
 * Delete an element from the circular queue. Return true if the operation is successful.
 * @return {boolean}
 */
MyCircularQueue.prototype.deQueue = function() {
	if (!this.isEmpty()) {
		this.queue[this.head] = null
		this.changeHeadPoint()

		return true
	}

	return false
}

/**
 * Get the front item from the queue.
 * @return {number}
 */
MyCircularQueue.prototype.Front = function() {
	if (!this.isEmpty()) {
		return this.queue[this.head]
	}

	return -1
}

/**
 * Get the last item from the queue.
 * @return {number}
 */
MyCircularQueue.prototype.Rear = function() {
	if (!this.isEmpty()) {
		return this.queue[this.tail]
	}

	return -1
}

/**
 * Checks whether the circular queue is empty or not.
 * @return {boolean}
 */
MyCircularQueue.prototype.isEmpty = function() {
	return this.head === -1 && this.tail === -1
}

/**
 * Checks whether the circular queue is full or not.
 * @return {boolean}
 */
MyCircularQueue.prototype.isFull = function() {
	return this.queue.indexOf(null) === -1
}

MyCircularQueue.prototype.changeHeadPoint = function() {
	// Is there an element left?
	if (this.head === this.tail) {
		this.head = -1
		this.tail = -1
	} else {
		this.head++

		// border
		if (this.head === this.queue.length) {
			this.head = 0
		}
	}
}

MyCircularQueue.prototype.changeTailPoint = function() {
	// empty array into the queue
	if (this.tail === -1 && this.head === -1) {
		this.tail++
		this.head++
	} else {
		this.tail++

		// border
		if (this.tail === this.queue.length) {
			this.tail = 0
		}
	}
}

var obj = new MyCircularQueue(3)
obj.enQueue(4)
obj.enQueue(5)
console.log('deQueue:', obj.deQueue())
console.log('Front:', obj.Front())
console.log('Rear:', obj.Rear())
console.log('isEmpty:', obj.isEmpty())
console.log('isFull:', obj.isFull())
