function Node(val,children) {
	this.val = val;
	this.children = children;
}
/**
 * @param {Node} root
 * @return {number[][]}
 */
const levelOrder = root => {
	if (root === null) return []

	var queue = [],
		resArr = []
	queue.push(root)
	while (queue.length > 0) {
		var tempArr = []
		var length = queue.length
		for (var i = 0; i < length; i++) {
			var tempNode = new Node()
			tempNode = queue.shift()
			for (let n of tempNode.children) {
				queue.push(n)
			}
			tempArr.push(tempNode.val)
		}
		resArr.push(tempArr)
	}
	return resArr
}

const tree = {
	val: 1,
	children: [
		{
			val: 3,
			children: [{ val: 5, children: [] }, { val: 6, children: [] }],
		},
		{ val: 2, children: [] },
		{ val: 4, children: [] },
	],
}

console.log(levelOrder(tree))
