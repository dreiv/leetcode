/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */
/**
 * @param {Node} root
 * @return {number[]}
 */
const preorder = root =>
	root
		? [
				root.val,
				...root.children.reduce(
					(acc, child) => acc.concat(preorder(child)),
					[]
				),
			]
		: []

const tree = {
	val: 1,
	children: [
		{
			val: 3,
			children: [{ val: 5, children: [] }, { val: 6, children: [] }],
		},
		{ val: 2, children: [] },
		{ val: 4, children: [] },
	],
}

console.log(preorder(tree))
