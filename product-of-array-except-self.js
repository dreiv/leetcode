/**
 * @param {number[]} nums
 * @return {number[]}
 */
const productExceptSelf = nums => {
	const result = []
	let productSoFar = 1

	for (let i = 0; i < nums.length; i++) {
		result[i] = productSoFar
		productSoFar *= nums[i]
	}

	productSoFar = 1
	for (let j = nums.length - 1; j >= 0; j--) {
		result[j] *= productSoFar
		productSoFar *= nums[j]
	}

	return result
}

console.log(productExceptSelf([1, 2, 3, 4]))
