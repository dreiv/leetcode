/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
const numJewelsInStones = function(J, S) {
	const map = new Set([...J])

	return [...S]
		.filter(s => map.has(s))
		.length
}

console.log(numJewelsInStones('aA', 'aAAbbbb'))
