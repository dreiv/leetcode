/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} L
 * @param {number} R
 * @return {number}
 */
var rangeSumBST = function(root, L, R) {
	if (!root) return null

	const result = []

	;(iterate = node => {
		if (!node) return

		if (node.val >= L && node.val <= R) {
			result.push(node.val)
		}

		iterate(node.left)
		iterate(node.right)
	})(root)

	return result.reduce((acc, curr) => acc + curr)
}

const bst = {
	val: 10,
	left: {
		val: 5,
		left: {
			val: 3,
		},
		right: {
			val: 7,
		},
	},
	right: {
		val: 15,
		right: {
			val: 18,
		},
	},
}

console.log(rangeSumBST(bst, 7, 15))
