/**
 * @param {number} x
 * @param {number} y
 * @return {number}
 */
const hammingDistance = (x, y) =>
	(x ^ y)
		.toString(2)
		.replace(/0/g, '')
		.length

// console.log(hammingDistance(1, 4))
console.log(hammingDistance(4, 2))
// console.log(hammingDistance(4, 14))
