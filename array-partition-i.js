/**
 * @param {number[]} nums
 * @return {number}
 */
const arrayPairSum = nums =>
	nums
		.sort((a, b) => a - b)
		.filter((_, index) => index % 2 === 0)
		.reduce((acc, val) => acc + val)

console.log(arrayPairSum([1, 4, 3, 2]))
