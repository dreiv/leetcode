/**
 * @param {string} s
 * @return {number}
 */
const romanToInt = s => {
	const d = { M: 1000, D: 500, C: 100, L: 50, X: 10, V: 5, I: 1 }
	let res = 0
	let p = 'I'

	for (let i = s.length - 1; i >= 0; i -= 1) {
		if (d[s[i]] < d[p]) {
			res -= d[s[i]]
		} else {
			res += d[s[i]]
		}

		p = s[i]
	}
	return res
}

console.log(romanToInt('III'))
// 3
console.log(romanToInt('IV'))
// 4
console.log(romanToInt('IX'))
// 9
console.log(romanToInt('LVIII'))
// 58
console.log(romanToInt('MCMXCIV'))
// 1994
