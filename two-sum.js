/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
const twoSum = (nums, target) => {
	const res = {}

	for (let i = 0; i < nums.length; i += 1) {
		const tmp = target - nums[i]
		if (nums[i] in res) {
			return [res[nums[i]], i]
		}
		res[tmp] = i
	}

	return null
}

console.log(twoSum([2, 7, 11, 15], 9))
// [0, 1]
