/**
 * @param {character[][]} board
 * @return {number}
 */
const numRookCaptures = board => {
	let rockX = 0,
		rockY = 0,
		count = 0

	board.find((subArr, idx) => {
		if (subArr.indexOf('R') !== -1) {
			rockX = subArr.indexOf('R')
			rockY = idx
			return true
		}
	})
	let vArr = []
	board.forEach(v => {
		vArr.push(v[rockX])
	})

	board[rockY].slice(0, rockX).lastIndexOf('p') >
	board[rockY].slice(0, rockX).lastIndexOf('B')
		? count++
		: 0

	let idx2 = board[rockY].slice(rockX).indexOf('p')
	if (
		idx2 !== -1 &&
		board[rockY]
			.slice(rockX)
			.slice(0, idx2)
			.indexOf('B') == -1
	)
		count++

	vArr.slice(0, rockY).lastIndexOf('p') >
	vArr.slice(0, rockY).lastIndexOf('B')
		? count++
		: 0

	let rp2 = vArr.slice(rockY).indexOf('p')
	let rb2 = vArr.slice(rockY).indexOf('B')
	if (rp2 !== -1 && (rb2 === -1 || rp2 < rb2)) count++

	return count
}

const board1 = [
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', 'p', '.', '.', '.', '.'],
	['.', '.', '.', 'R', '.', '.', '.', 'p'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', 'p', '.', '.', '.', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
]

const board2 = [
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', 'p', 'p', 'p', 'p', 'p', '.', '.'],
	['.', 'p', 'p', 'B', 'p', 'p', '.', '.'],
	['.', 'p', 'B', 'R', 'B', 'p', '.', '.'],
	['.', 'p', 'p', 'B', 'p', 'p', '.', '.'],
	['.', 'p', 'p', 'p', 'p', 'p', '.', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
]

const board3 = [
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', 'p', '.', '.', '.', '.'],
	['.', '.', '.', 'p', '.', '.', '.', '.'],
	['p', 'p', '.', 'R', '.', 'p', 'B', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
	['.', '.', '.', 'B', '.', '.', '.', '.'],
	['.', '.', '.', 'p', '.', '.', '.', '.'],
	['.', '.', '.', '.', '.', '.', '.', '.'],
]

console.log(numRookCaptures(board1))
console.log(numRookCaptures(board2))
console.log(numRookCaptures(board3))
