const isCommonPrefix = (strs, len) => {
	const str1 = strs[0].substring(0, len)

	for (let i = 1; i < strs.length; i++) {
		if (!strs[i].startsWith(str1)) {
			return false
		}
	}

	return true
}

/**
 * @param {string[]} strs
 * @return {string}
 */
const longestCommonPrefix = function(strs) {
	if (strs == null || strs.length == 0) return ""
	let minLen = Number.MAX_VALUE

	for (str of strs) {
		minLen = Math.min(minLen, str.length)
	}
	let low = 1
	let high = minLen

	while (low <= high) {
		const middle = (low + high) / 2
		if (isCommonPrefix(strs, middle)) {
			low = middle + 1
		} else {
			high = middle - 1
		}
	}

	return strs[0].substring(0, (low + high) / 2)
}

console.log(longestCommonPrefix(["flower", "flow", "flight"]))
// "fl"

console.log(longestCommonPrefix(["dog", "racecar", "car"]))
// ""
