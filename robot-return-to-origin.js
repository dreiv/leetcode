/**
 * @param {string} moves
 * @return {boolean}
 */
const judgeCircle = moves => {
	let x = 0, y = 0

	for (move of moves) {
		switch (move) {
			case 'U':
				x++
				break;
			case 'R':
				y++
				break;
			case 'D':
				x--
				break;
			case 'L':
				y--
				break;

			default:
				break;
		}
	}

	return !x && !y
}

console.log(judgeCircle('UD'))
// console.log(judgeCircle('LL'))