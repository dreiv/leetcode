/**
 * @param {number} x
 * @return {number}
 */
const reverse = x => {
	const maxRange = 2 ** 31 - 1
	const minRange = -(2 ** 31)
	let result = 0
	let toReverse = x

	while (toReverse !== 0) {
		result = result * 10 + (toReverse % 10)
		toReverse = parseInt(toReverse / 10)
	}

	if (result > maxRange || result < minRange) return 0

	return result
}

console.log(reverse(123))
// 321
console.log(reverse(-123))
// -321
console.log(reverse(120))
// 21
console.log(reverse(1534236469))
// 9646324351
