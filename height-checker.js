/**
 * @param {number[]} heights
 * @return {number}
 */
const heightChecker = heights =>
	[...heights].sort((a, b) => a - b)
	.reduce((acc, curr, idx) =>
		acc + (curr != heights[idx])
	, 0)

console.log(heightChecker([1, 1, 4, 2, 1, 3]))
