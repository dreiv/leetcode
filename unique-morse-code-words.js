const morse = {
	a: '.-',
	b: '-...',
	c: '-.-.',
	d: '-..',
	e: '.',
	f: '..-.',
	g: '--.',
	h: '....',
	i: '..',
	j: '.---',
	k: '-.-',
	l: '.-..',
	m: '--',
	n: '-.',
	o: '---',
	p: '.--.',
	q: '--.-',
	r: '.-.',
	s: '...',
	t: '-',
	u: '..-',
	v: '...-',
	w: '.--',
	x: '-..-',
	y: '-.--',
	z: '--..'
}

const toMorse = word =>
	[...word].map(c => morse[c]).join('')

/**
 * @param {string[]} words
 * @return {number}
 */
const uniqueMorseRepresentations = words =>
	new Set(words.map(toMorse)).size

console.log(uniqueMorseRepresentations(['gin', 'zen', 'gig', 'msg']))
