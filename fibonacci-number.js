/**
 * @param {number} num
 * @return {number}
 */
const fib = num => {
	const cache = {}

	return (nthFib = n => {
		if (n < 2) return n
		if (typeof cache[n] === 'undefined')
			cache[n] = nthFib(n - 1) + nthFib(n - 2)
		return cache[n]
	})(num)
}

console.log(fib(3))
