/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
const isUnivalTree = root => {
	let flag = true

	;(iterate = (node, val) => {
		if (node.val !== val) flag = false

		if (node.left) iterate(node.left, val)
		if (node.right) iterate(node.right, val)
	})(root, root.val)

	return flag
}

const tree = {
	val: 1,
	left: {
		val: 1,
		left: {
			val: 1
		},
		right: {
			val: 1
		}
	},
	right: {
		val: 1,
		right: {
			val: 1
		}
	}
}

console.log(isUnivalTree(tree))
