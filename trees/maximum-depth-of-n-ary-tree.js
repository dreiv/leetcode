/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */
/**
 * @param {Node} root
 * @return {number}
 */
const maxDepth = root => {
	if (!root) return 0
	let max = 0

	;(iterate = (node, level) => {
		if (!node) return

		if (level > max) max = level
		node.children.forEach(child => {
			iterate(child, level + 1)
		})
	})(root, 1)

	return max
}

const tree = {
	val: 1,
	children: [
		{
			val: 3,
			children: [{ val: 5, children: [] }, { val: 6, children: [] }]
		},
		{ val: 2, children: [] },
		{ val: 4, children: [] }
	]
}
console.log(maxDepth(tree))
