/**
 * @param {string[]} arr
 * @return {string[]}
 */
const commonChars = arr => {
	let ans = arr[0].split('')
	for (let i = 1; i < arr.length; i++) {
		ans = findCommon(ans, arr[i].split(''))
	}
	return ans
}

const findCommon = (a, b) => {
	return a.filter(val => {
		const i = b.indexOf(val)
		if (i !== -1) {
			b.splice(i, 1)
			return true
		}
		return false
	})
}

console.log(commonChars(['bella', 'label', 'roller']))
console.log(commonChars(['cool', 'lock', 'cook']))
